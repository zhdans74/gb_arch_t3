# Выбор системы для реализации шины данных

| Область сравнения                                        | Kafka                               | Оценка | RabbitMQ                                                           | Оценка |
|:---------------------------------------------------------|:------------------------------------|:------:|:-------------------------------------------------------------------|:------:|
| Распределенность системы                                 | В базе, с множеством настроек       |   5    | Дополнительные манипуляции с кластеризацией и сложности репликации |   2    |
| Возможность перечитать сообщения                         | Да, на время сохранности лога       |   5    | Через новую публикацию                                             |   0    |
| Распределенное считывание                                | На уровне партиций для одной группы |   3    | В рамках одной очереди                                             |   5    |
| Библиотека от разработчика для встраивания в приложениях | Да                                  |   5    | Да                                                                 |   5    |
| Гарантия доставки                                        | Да                                  |   5    | Да                                                                 |   5    |
| Скорость обработки                                       | 1 million messages per second       |   5    | 4K-10K messages per second                                         |   3    |
| _Итого_                                                  |                                     |   28   |                                                                    |   20   |
