openapi: '3.0.2'
info:
  title: CRM
  description: CRM Rostelecom.
  version: '1.0'
servers:
  - url: https://crm.rostelecom.ru
paths:
  /orders:
    get:
      tags:
        - Order
      summary: Get orders.
      parameters:
        - in: query
          name: dateFrom
          schema:
            type: string
            format: date
          required: false
          description: Order date from.
          example: 15.05.2022
        - in: query
          name: dateTo
          schema:
            type: string
            format: date
          required: false
          description: Order date to.
          example: 16.05.2022
      responses: 
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Order"
              example:
                - OrderId: 1
                  Number: "1"
                  Date: 16.05.2022
                  CustomerId: 10
                  Products:
                    - OrderId: 1
                      ProductId: 1
                      Product:
                        ProductId: 1
                        Name: "Подключение интернета"
                        TerritoryId: 10
                    - OrderId: 1
                      ProductId: 2
                      Product:
                        ProductId: 2
                        Name: "Подключение телевидения"
                        TerritoryId: 10
        '401':
          $ref: '#/components/responses/UnauthorizedError'                        
    post:
      tags:
        - Order
      summary: Create order.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Order"
            example:
              OrderId: 0
              Number: "1"
              Date: 16.05.2022
              CustomerId: 10
              Products:
                - OrderId: 0
                  ProductId: 1
                  Product:
                    ProductId: 1
                    Name: "Подключение интернета"
                    TerritoryId: 10
                - OrderId: 0
                  ProductId: 2
                  Product:
                    ProductId: 2
                    Name: "Подключение телевидения"
                    TerritoryId: 10
      responses: 
        '200':
          description: Success                  
        '401':
          $ref: '#/components/responses/UnauthorizedError'

  /orders/{customerId}:
    get:
      tags:
        - Order
      summary: Get orders by customer.
      parameters:
        - in: path
          name: customerId
          schema:
            type: integer
          required: true
          description: Numeric customer ID for receiving orders.
          example: 10
      responses: 
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Order"
              example:
                OrderId: 1
                Number: "1"
                Date: 16.05.2022
                CustomerId: 10
                Products:
                  - OrderId: 1
                    ProductId: 1
                    Product:
                      ProductId: 1
                      Name: "Подключение интернета"
                      TerritoryId: 10
                  - OrderId: 1
                    ProductId: 2
                    Product:
                      ProductId: 2
                      Name: "Подключение телевидения"
                      TerritoryId: 10
        '401':
          $ref: '#/components/responses/UnauthorizedError'

  /products/{territoryId}:
    get:
      tags:
        - Product
      summary: Get products by territory.
      parameters:
        - in: path
          name: territoryId
          schema:
            type: integer
          required: true
          description: Numeric territory ID for receiving products.
          example: 10
      responses: 
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Product"
              example:
                - ProductId: 1
                  Name: "Подключение интернета"
                  TerritoryId: 10
                - ProductId: 2
                  Name: "Подключение телевидения"
                  TerritoryId: 10                
        '401':
          $ref: '#/components/responses/UnauthorizedError'
          
components:
  schemas:
    Order:
      type: object
      properties: 
        OrderId:
          type: integer
          nullable: false
        Number:
          type: string
          nullable: false
        Date:
          type: string
          format: date
          nullable: false
        CustomerId:
          type: integer 
          nullable: false
        Products:
            type: array
            items:
              $ref: "#/components/schemas/OrderProduct"
    Product:
      type: object
      properties: 
        ProductId:
          type: integer
          nullable: false
        Name:
          type: string
          nullable: false
        TerritoryId:
          type: integer 
          nullable: false   
    OrderProduct:
      type: object
      properties: 
        ProductId:
          type: integer
          nullable: false
        Product:
          $ref: "#/components/schemas/Product"
        OrderId:
          type: integer
          nullable: false
  securitySchemes:
    bearerAuth:           
      type: http
      scheme: bearer
      bearerFormat: JWT        
  responses:
    UnauthorizedError:
      description: Access token is missing or invalid
security:
  - bearerAuth: []