openapi: '3.0.2'
info:
  title: Task manager service API
  description: The service is intended for task management.
  version: '1.0'
servers:
  - url: https://tm.rostelecom.ru/v1
paths:
  /tasks:
    get:
      tags:
        - Task
      summary: Get tasks.
      parameters:
        - in: query
          name: dateFrom
          schema:
            type: string
            format: date
          required: false
          description: Task date from.
          example: 15.05.2022
        - in: query
          name: dateTo
          schema:
            type: string
            format: date
          required: false
          description: Task date to.
          example: 16.05.2022
      responses: 
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Task"
              example:
                - Id: 10
                  Name: "Выполнить настройку интернета для клиента №1056"
                  DateCreate: 16.05.2022
                  Description: "Выполнить настройку интернета для клиента №1056"
                  AuthorId: 15
                  Author: "Петров Олег Петрович"
                  PerformerId: 10
                  Performer: "Сидоров Иван Иванович"
                  AppealId: 5
                  WorkflowId: 1
                  Status: New

        '401':
          $ref: '#/components/responses/UnauthorizedError'        
    post:
      tags:
        - Task
      summary: Create task.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Task"
            example:
              Id: 0
              Name: "Выполнить настройку интернета для клиента №1056"
              dateFrom:
              Description: "Выполнить настройку интернета для клиента №1056"
              AuthorId: 15
              Author: "Петров Олег Петрович"
              PerformerId: 0
              Performer: ""
              AppealId: 5
              WorkflowId: 1
              Status: New
      responses: 
        '200':
          description: Success                  
        '401':
          $ref: '#/components/responses/UnauthorizedError'              

  /tasks/{boardId}:
    get:
      tags:
        - Task
      summary: Get board tasks.
      parameters:
        - in: path
          name: BoardId
          schema:
            type: integer
          required: true
          description: Board ID.
          example: 1
      responses: 
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Task"
              example:
                - Id: 10
                  Name: "Выполнить настройку интернета для клиента №1056"
                  DateCreate: 16.05.2022
                  Description: "Выполнить настройку интернета для клиента №1056"
                  AuthorId: 15
                  Author: "Петров Олег Петрович"
                  PerformerId: 10
                  Performer: "Сидоров Иван Иванович"
                  AppealId: 5
                  WorkflowId: 1
                  Status: InProcess
        '401':
          $ref: '#/components/responses/UnauthorizedError'          

  /boards:
    post:
      tags:
        - Board
      summary: Create board.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Board"
            example:
              Id: 0
              Name: "Доска команды маркетинга"  
              Columns: []
              Tasks: []
      responses: 
        '200':
          description: Success                  
        '401':
          $ref: '#/components/responses/UnauthorizedError'

  /boards/{boardId}:
    get:
      tags:
        - Board
      summary: Get board.
      parameters:
        - in: path
          name: BoardId
          schema:
            type: integer
          required: true
          description: Board ID.
          example: 1
      responses: 
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Task"
              example:
                - Id: 1
                  Name: "Доска команды маркетинга"  
                  Columns:
                    - Id: 1
                      BoardId: 1
                      Name: ToDo
                      TaskStatus: New
                    - Id: 2
                      BoardId: 1
                      Name: In Process
                      TaskStatus: InProcess      
                    - Id: 3
                      BoardId: 1
                      Name: Closed
                      TaskStatus: Closed                                         
                  Tasks: 
                    - Id: 10
                      Name: "Выполнить настройку интернета для клиента №1056"
                      Description: "Выполнить настройку интернета для клиента №1056"
                      AuthorId: 15
                      Author: "Петров Олег Петрович"
                      PerformerId: 10
                      Performer: "Сидоров Иван Иванович"
                      AppealId: 5
                      WorkflowId: 1
                      Status: InProcess 

        '401':
          $ref: '#/components/responses/UnauthorizedError'     
    post:
      tags:
        - Board
      summary: Add column on board.
      parameters:
        - in: path
          name: boardId
          schema:
            type: integer
          required: true
          description: Board Id.
          example: 1
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/BoardColumn"
            example:
              Id: 0
              BoardId: 1
              Name: In Process
              Status: InProcess
      responses: 
        '200':
          description: Success                  
        '401':
          $ref: '#/components/responses/UnauthorizedError'        

  /boards/{boardId}/tasks:    
    post:
      tags:
        - Board
      summary: Create task on board.
      parameters:
        - in: path
          name: boardId
          schema:
            type: integer
          required: true
          description: Board Id.
          example: 1
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Task"
            example:
              Id: 0
              Name: "Выполнить настройку ТВ для клиента №1056"
              dateFrom:
              Description: "Выполнить настройку ТВ для клиента №1056"
              AuthorId: 15
              Author: "Петров Олег Петрович"
              PerformerId: 0
              Performer: ""
              AppealId: 6
              WorkflowId: 1
              Status: New
      responses: 
        '200':
          description: Success                  
        '401':
          $ref: '#/components/responses/UnauthorizedError'  

components:
  schemas:
    Task:
      type: object
      properties: 
        Id:
          type: integer
          nullable: false
        Name:
          type: string
          nullable: false
        DateCreate:
          type: string
          nullable: false
          format: date
        Description:
          type: string
          nullable: true          
        AuthorId:
          type: integer
          nullable: false
        Author:
          type: string
          nullable: false
        PerformerId:
          type: integer
          nullable: true
        Performer:
          type: string
          nullable: true
        AppealId:
          type: integer
          nullable: true            
        WorkflowId:
          type: integer
          nullable: false                 
        Status: 
          type: string 
          nullable: false 
          enum:
            - New
            - InProcess
            - Closed  

    Board:
      type: object
      properties: 
        Id:
          type: integer
          nullable: false
        Name:
          type: string
          nullable: false
        Columns: 
          type: array
          items:
            $ref: "#/components/schemas/BoardColumn"
        Tasks: 
          type: array
          items:
            $ref: "#/components/schemas/Task"

    BoardColumn:
      type: object
      properties:
        Id:
          type: integer
          nullable: false
        BoardId:
          type: integer
          nullable: false
        Name:
          type: string
          nullable: false
        TaskStatus: 
          type: string 
          nullable: false 

  securitySchemes:
    bearerAuth:           
      type: http
      scheme: bearer
      bearerFormat: JWT        
  responses:
    UnauthorizedError:
      description: Access token is missing or invalid
      
security:
  - bearerAuth: []